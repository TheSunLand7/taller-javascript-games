/**Initial settings */
let { log, error, warn, group, groupEnd, table } = console;

/**Main code */
const canvas = document.getElementById('game');
const lienzo = canvas.getContext('2d'); //Area a dibujar
const btnUp = document.getElementById('up');
const btnDown = document.getElementById('down');
const btnLeft = document.getElementById('left');
const btnRight = document.getElementById('right');
const vidas = document.getElementById('lives');
const time = document.getElementById('time');
const record = document.getElementById('best-time');
const result = document.getElementById('result');

let canvasSize;
let elementSize;
const enemiesPosition = new Set();
let level = 0;
let lives = 3;

let timeStart;
let timePlayer;
let timeInterval;

const playerPosition = new Map();
playerPosition.set('x', undefined);
playerPosition.set('y', undefined);

const giftPosition = new Map();
giftPosition.set('x', undefined);
giftPosition.set('y', undefined);

// const doorPosition = new Map();
// doorPosition.set('x', undefined);
// doorPosition.set('y', undefined);

window.addEventListener('load', setCanvasSize);
window.addEventListener('resize', setCanvasSize);

function startGame() {
    lienzo.font = elementSize + 'px Verdana';
    lienzo.textAlign = 'end';

    //Cada string se transforma en un array y este array contiene otro array con cada carácter del string
    //para ver el resultado se puede usar un console.log(mapRowsColsBi)
    const map = maps[level];
    if (!map) {
        gameWin();
        return;
    }

    if (!timeStart) {
        timeStart = Date.now();
        timeInterval = setInterval(showTime, 100);
        showBestTime();
    }

    const mapRowsColsBi = map
        .match(/[IOX\-]+/g) //Búscame todo lo que empiece por I,X,O o '-', pero como el '-' es una palabra reservada usamos el slash invertido
        .map(row => row.split(''));

    showLives();
    lienzo.clearRect(0, 0, canvasSize, canvasSize);
    enemiesPosition.clear();

    //Crear un array de arrays (una matriz)
    mapRowsColsBi.forEach((row, x) => {
        row.forEach((col, y) => {
            let posx = elementSize * (y + 1); //Para que los elemetos se vean. Si se inicia en 0, los elementos salen del canvas.
            let posy = elementSize * (x + 1);
            lienzo.fillText(emojis[col], posx, posy);

            if (col === 'O') {
                if (!playerPosition.get('x') && !playerPosition.get('y')) {
                    playerPosition.set('x', posx);
                    playerPosition.set('y', posy);
                }
                // doorPosition.set('x', posx);
                // doorPosition.set('y', posy);

            } else if (col === 'I') {
                giftPosition.set('x', posx);
                giftPosition.set('y', posy);
            } else if (col === 'X') {
                enemiesPosition.add({
                    x: posx,
                    y: posy,
                });
            }
        });
    });

    movePlayer();
}

function movePlayer() {
    const giftCollitionX = playerPosition.get('x').toFixed(2) === giftPosition.get('x').toFixed(2);
    const giftCollitionY = playerPosition.get('y').toFixed(2) === giftPosition.get('y').toFixed(2);
    if (giftCollitionX && giftCollitionY) {
        levelWin();
    }

    enemiesPosition.forEach(item => {
        const playerCollitionX = playerPosition.get('x').toFixed(2) === item.x.toFixed(2);
        const playerCollitionY = playerPosition.get('y').toFixed(2) === item.y.toFixed(2);
        if (playerCollitionX && playerCollitionY) restartLevel();
    });

    lienzo.fillText(emojis['PLAYER'], playerPosition.get('x'), playerPosition.get('y'));
}

function levelWin() {
    log('Subiste de nivel!')
    level++;
    startGame();
}

function gameWin() {
    clearInterval(timeInterval);
    const playerTime = Date.now() - timeStart;
    const recordTime = localStorage.getItem('record_time');
    if (recordTime) {
        if (playerTime < recordTime) {
            localStorage.setItem('record_time', playerTime);
            result.innerHTML = 'SUPERASTE el record! :)';
        } else result.innerHTML = 'Lo siento, no superaste el record! :(';
    } else {
        localStorage.setItem('record_time', playerTime);
        result.innerHTML = "¿Primera vez? :v"
    }
}

function restartLevel() {
    log('Perdiste!');
    // playerPosition.set('x', doorPosition.get('x'));
    // playerPosition.set('y', doorPosition.get('y'));

    lives--;
    if (lives <= 0) {
        level = 0;
        lives = 3;
        timeStart = undefined;
    }
    playerPosition.set('x', undefined);
    playerPosition.set('y', undefined);
    startGame();

}

function showLives() {
    const hearts = new Array(lives).fill(emojis['HEART']);
    vidas.innerHTML = hearts.join('');
}

function showTime() {
    time.innerHTML = Date.now() - timeStart;
}

function showBestTime() {
    record.innerHTML = localStorage.getItem('record_time');
}

function fixNumber(number) {
    return +number.toFixed(2);
}

function setCanvasSize() {
    (window.innerHeight > window.innerWidth)
        ? canvasSize = +(window.innerWidth * 0.8).toFixed(0)
        : canvasSize = +(window.innerHeight * 0.8).toFixed(0);

    canvas.setAttribute('width', canvasSize);
    canvas.setAttribute('height', canvasSize);

    elementSize = +(canvasSize / 10).toFixed(0);

    playerPosition.set('x', undefined);
    playerPosition.set('y', undefined);
    startGame();
}

btnUp.addEventListener('click', moveUp);
btnDown.addEventListener('click', moveDown);
btnLeft.addEventListener('click', moveLeft);
btnRight.addEventListener('click', moveRight);
window.addEventListener('keyup', moveByKeys);

function moveUp() {
    let move = playerPosition.get('y') - elementSize;
    (move < elementSize)
        ? playerPosition.set('y', playerPosition.get('y'))
        : playerPosition.set('y', move);
    startGame();
}

function moveDown() {
    let move = playerPosition.get('y') + elementSize;
    (move > canvasSize)
        ? playerPosition.set('y', playerPosition.get('y'))
        : playerPosition.set('y', move);
    startGame();

}

function moveLeft() {
    let move = playerPosition.get('x') - elementSize;
    (move < elementSize)
        ? playerPosition.set('x', playerPosition.get('x'))
        : playerPosition.set('x', move);
    startGame();

}

function moveRight() {
    let move = playerPosition.get('x') + elementSize;
    (move > canvasSize)
        ? playerPosition.set('x', playerPosition.get('x'))
        : playerPosition.set('x', move);
    startGame();

}

function moveByKeys(event) {
    switch (event.key) {
        case 'ArrowUp':
            moveUp();
            break;
        case 'ArrowLeft':
            moveLeft();
            break;
        case 'ArrowRight':
            moveRight();
            break;
        case 'ArrowDown':
            moveDown();
            break;
        default:
            log('No se que tecla has pulsado');
            break;
    }
}